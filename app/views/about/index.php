<div class="container mt-4">
  <img src="<?= base_url.'img/profile.jpg' ?>" alt="profile" srcset="" class="rounded-circle" width="200">
  <h2>About me</h2>
  <p>Halo, nama saya <?= $data['nama'] ?>, saya bekerja sebagai <?= $data['pekerjaan'] ?>, saya berumur <?= $data['umur'] ?></p>
</div>